package com.tbox.doormanapp;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.doorman_test.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {
	EditText tf_username;
	EditText tf_password;
	Button btn;
	ProgressBar spinner;

	
	// private methods
	private void registerAllElements(){
		tf_username = (EditText) findViewById(R.id.user_id_text);
	    tf_password = (EditText) findViewById(R.id.user_password_text);
	    btn = (Button)findViewById(R.id.btn_login);
	    spinner = (ProgressBar)findViewById(R.id.progressBar1);
	}
	private void setListeners(){
		btn.setOnClickListener(this);
	}
	
	private void checkIfLogin(){
		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
		if(settings.getString("key", null)!=null && settings.getString("property_id", null)!=null)
		{
			Intent home=new Intent(LoginActivity.this,GuestListActivity.class);
			startActivity(home);
			finish();
			
		}
	}
	private boolean isFieldValidated(){
		if(tf_username.getText().length()==0 || tf_password.getText().length()==0){
			return false;
		}
		return true;
	}
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		
		registerAllElements();
		setListeners();
		checkIfLogin();
	}

	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.btn_login:
			
//			JSONArray array=new JSONArray();
//			JSONObject obj1 = new JSONObject();
//			try {
//				obj1.put("test", "1");
//				obj1.put("test2", "2");
//			} catch (JSONException e2) {
//				// TODO Auto-generated catch block
//				e2.printStackTrace();
//			}
//			
//			array.put(obj1);
//
//			ArrayList<JSONObject> userList = new ArrayList<JSONObject>();
//			for(int i=0;i<array.length();i++){
//				try {
//					userList.add(array.getJSONObject(i));
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//			
//			// How to store JSON string
//			Gson gson = new Gson();
//
//			String json = gson.toJson(userList);
//			
//			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
//			Editor editor = prefs.edit();
//			editor.putString(Constants.kCurrentUsersList, json);
//			editor.commit();
//				
//			
//			String testString= prefs.getString(Constants.kCurrentUsersList, null);
//			
//			
//			@SuppressWarnings("unchecked")
//			ArrayList<JSONObject> newuserList = gson.fromJson(testString, ArrayList.class);
			
			
			if(isFieldValidated()){
				spinner.setVisibility(View.VISIBLE);
				JSONObject obj = new JSONObject();
				try {
					obj.put("email_id",tf_username.getText().toString());
					obj.put("password",tf_password.getText().toString());
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				RequestParams params = new RequestParams();
				params.put("data",obj.toString());
				
    	      //send post request for user login
    	    	AsyncHttpClient client = new AsyncHttpClient();
    	    	client.post(Constants.api_base_url+Constants.doorman_login, params , new JsonHttpResponseHandler(){
    	    		@Override
                    public void onSuccess(final JSONObject object){
    	    			int status = 0;
    					try {
    						status = object.getInt("status");
		   					if (status == 1){
			   					Intent openGestListActivity=new Intent(LoginActivity.this,GuestListActivity.class);
			   		        	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
			   		    		SharedPreferences.Editor editor = settings.edit();
			   		    		editor.putString("key",object.getString("api_key")).commit();
			   		    		editor.putString("property_id",object.getString("property_id")).commit();
			   		    		editor.putString("type",object.getString("type")).commit();
			   		    		startActivity(openGestListActivity);
			   		    		finish();
		   					}else if (status ==  0 ){
								String strName= object.getString("error_message");
								Toast.makeText( getApplicationContext() , strName , Toast.LENGTH_SHORT).show();
		   					}
    					} catch (JSONException e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
    	    		}
    	    	     @Override
		             protected void handleFailureMessage(Throwable e, String responseBody) {
		            	 spinner.setVisibility(View.INVISIBLE);
		 				try{
							JSONObject jobject =  new JSONObject(responseBody);
							int status = jobject.getInt("status");
							if(status ==0){
//								UserInfo userInfo= UserInfo.getInstance(context);	
								ImageViewActivity.showAlertDialog(jobject.getString("error_message"), LoginActivity.this);
//								userInfo.saveUserInfo(context);
							}
						} catch (JSONException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
		             }
    	    	});
	
			}
		}
	}
}
		
