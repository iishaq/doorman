package com.tbox.doormanapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.StringTokenizer;

import org.json.JSONException;
import org.json.JSONObject;


public class AlertSoundPlayer implements FileDownloaderInterface {

	public String mvoice_path=null;
	public Context context;
	public JSONObject _user;
	int playcount=1;
	int value=0;
	 FileChannel channel =null;
		// Get an exclusive lock on the whole file
	 FileLock lock = null;
	
	public MediaPlayer mp1 =null;
	MediaPlayer mp2 =null;
	
	@SuppressLint("SdCardPath")
	@Override
	public void fileDownloaded(FileDownloaderInterface callBack) {
		// TODO Auto-generated method stub
		
		// play hardcoded voice
//		Uri path = Uri.parse("android.resource://com.tbox.doorman"+"/"+ R.raw.testingfinal);
		
//		File f = new File("android.resource://com.tbox.doorman"+ "/"+ R.raw.testingfinal);
		//if file exist and no other file is playing
//		if(f.exists()){
		
		Resources res = context.getResources();
		String pkgName = context.getPackageName();
		
		String id = "new_guest";
		String id_second = "register_guest";
        // Get raw resource ID
		int identifier;
		if(value==0)
			identifier = res.getIdentifier(id, "raw", pkgName);
		else
			identifier = res.getIdentifier(id_second, "raw", pkgName);
        AssetFileDescriptor afd = res.openRawResourceFd(identifier);
        
   	 	try {
   	 		if(mp1==null){
   	 			mp1 = new MediaPlayer();
   	 		}
   		 
   		mp1.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
   		afd.close();//just added this line**
//   		 mp.prepare();
   		mp1.prepareAsync();
   		mp1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
   		    @Override
   		    public void onPrepared(MediaPlayer mp) {
   		    	mp1.start();
   		    }
   		});
   		
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
           e.printStackTrace();
			}
   	 	
	   	
			
			mp1.setOnCompletionListener(new OnCompletionListener() {
					
					@Override
					public void onCompletion(MediaPlayer mp) {
						if(playcount==2){
							return;
						}
						playcount++;
						
						File f = new File("/sdcard/doorman", mvoice_path);
						//if file exist and no other file is playing
						if(f.exists()){
							try {
								if (mp.isPlaying()) {
						            mp.stop();
						        }
                                mp.reset();
                                mp.setDataSource(f.getAbsolutePath());
                            } catch (Exception e1) {
                                e1.printStackTrace();
                                System.exit(-1);
                                Log.d("testing", "testing");
                            }
							
							mp.prepareAsync();
					   		mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
					   		    @Override
					   		    public void onPrepared(MediaPlayer mp) {
					   		    	mp.start();
					   		    }
					   		});
						}
					}
			});
			
         }
	
	@SuppressLint("SdCardPath")
	public void playAlertSoundForVoice(JSONObject _user,Context _context,String str){
		context=_context;
		if(str.equals("1"))
			value=1;
		else 
			value=0;
		try {
			String voice_url = _user.getString(Constants.kuser_name_recording_path);
			if(voice_url == null || voice_url.length()==0){
				return;
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
				
		
		try {
				String file_path = _user.getString(Constants.kuser_name_recording_path);
				String user_id =_user.getString(Constants.kuser_id);
				String[] split = file_path.split("upload_voice");
				String filenamewithextention = split[1];
				
				StringTokenizer tokens = new StringTokenizer(filenamewithextention, ".");
				String _filename = tokens.nextToken();
				
				
				String file_name = _filename+"_"+user_id+".mp3";
				mvoice_path = file_name;
				File f = new File("/sdcard/doorman", file_name);
				if(!f.exists()){
					
					
					//check for previous voice file for same user, if there is then delete it and download, if not then simply download
					File doormanDir = new File("/sdcard/doorman");
					try{
						
					}catch (NullPointerException e) {
						// TODO: handle exception
						for (File file : doormanDir.listFiles()) {
						    if (file.isFile()){
						    	String name = file.getName();
						    	String[] _split = name.split("_");
						    	String _user_id = _split[1];
								StringTokenizer _tokens = new StringTokenizer(_user_id, ".");
								_user_id = _tokens.nextToken();
								if(user_id.endsWith(_user_id)){
									File _f = new File("/sdcard/doorman/", name);
									if(_f.exists()){
										_f.delete();
									}
								}
						    }
						}
					}
					
					
			         FileDownloader fileDownloader = new FileDownloader();
			         fileDownloader.callBack=this;
			         fileDownloader.fileName = file_name;
			         fileDownloader.filePath = "/doorman";
			         fileDownloader.execute(file_path);
					
				}else{
					fileDownloaded(null);
				}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
}
