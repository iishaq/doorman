package com.tbox.doormanapp;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;

import android.os.AsyncTask;

public class JsonFetcher extends AsyncTask<JSONArray,Void,JSONArray> {
	
	List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
	public int st;
	public String res;
	HashMap<String, String> map = new HashMap<String, String>();
	static InputStream is = null;
	String url=null;
    String method = null;
    public JsonFetcher(String URL, String method, List<NameValuePair> paramsPar) {
        url=URL;
        paramsList=paramsPar;
        this.method = method;
        
    }
	
	/*@Override
	protected Object doInBackground(Object... params) {
		// TODO Auto-generated method stub
		JSONArray jArray = JsonClass.makeHttpRequest(url, method, paramsList);
		int Id=0;
	
		//String message; 
        try {
            JSONObject jobject = jArray.getJSONObject(0);
           
            st=jobject.getInt("status");
            if(st==0)
            {
            res=jobject.getString("success_message");
            }
            map.put("status",""+jobject.getInt("status"));
           
                   
            } catch (JSONException e) {
            Id=2;  
            e.printStackTrace();
        }
        return Id;
	} protected void  onPostExecute(Object result) {
		
		
	}*/

	@Override
	protected JSONArray doInBackground(JSONArray... params) {
		// TODO Auto-generated method stub
		JSONArray jArray = JsonClass.makeHttpRequest(url, method, paramsList);
		return jArray;
	}
	protected void  onPostExecute(JSONArray result) {
	 
	
	}

}
