package com.tbox.doormanapp;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.doorman_test.R;
import com.tbox.imagedownloader.ImageLoader;

import android.R.bool;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class GridViewAdapterGuest extends ArrayAdapter<JSONObject> implements FileDownloaderInterface{
	private Context context;
	private ArrayList<JSONObject> _userList;
	boolean audioPlaying = false;
	MediaPlayer mp = new MediaPlayer();
	File f = null;
	
	private Boolean isRedBgNeeded(JSONObject userState, String userID){
		long timeStamp = 0;
		try {
			timeStamp = userState.getLong(userID);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

				//3000(millliseconds in a second)*60(seconds in a minute)*5(number of minutes)=300000
			long diff = System.currentTimeMillis()-timeStamp;
				if (diff<300000){ 
				    //server timestamp is within 5 minutes of current system time
					return true;
				} else {
				    //server is not within 5 minutes of current system time
					return false;
				}
		
	}
	
	GridViewAdapterGuest(Context context, int resourceId,
			ArrayList<JSONObject> items){
		super(context, resourceId, items);
		this.context = context;
		_userList = items;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return _userList.size();
	}

	@Override
	public JSONObject getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.guest_list_activity,
					null);
		}
		convertView.setTag(position);
		final RelativeLayout rl_image_frame = (RelativeLayout) convertView.findViewById(R.id.user_img_layout);
		convertView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// check if is new the make background transparent
				try {
					String userId = _userList.get((Integer) v.getTag())
							.getString(Constants.kuser_id);
					String isNew = ServiceDoorman.newUserStates
							.getString(userId);
					if (isNew.equalsIgnoreCase("isNew")) {
						ServiceDoorman.newUserStates
								.put(userId, "isNotNew");
						if(_userList.get((Integer) v.getTag()).getString(Constants.kcheckinout).equals("1"))
							rl_image_frame.setBackgroundColor(Color.GREEN);
						else	
							rl_image_frame.setBackgroundColor(Color.TRANSPARENT);
					}
				} catch (JSONException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}

			}
		});

		TextView tv_username = (TextView) convertView
				.findViewById(R.id.guestUserName);
		
//		TextView tv_distance = (TextView) convertView
//				.findViewById(R.id.distanceGuest);
//		TextView tv_like = (TextView) convertView.findViewById(R.id.like);
//		TextView tv_reassOn = (TextView) convertView
//				.findViewById(R.id.reasson);
//		TextView tv_title = (TextView) convertView.findViewById(R.id.title);
		ImageView img_view = (ImageView) convertView
				.findViewById(R.id.img_view);
		ImageView img_super_user_star = (ImageView) convertView.findViewById(R.id.img_super_user);
		try {
			if(_userList.get(position).getString(Constants.ksuper_user).equals("1"))
				img_super_user_star.setVisibility(View.VISIBLE);
			else
				img_super_user_star.setVisibility(View.INVISIBLE);
			img_view.setTag(position);
		} catch (JSONException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		img_view.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences settings = PreferenceManager
						.getDefaultSharedPreferences(context);
				
				String type=settings.getString("type", null);
				if(!type.equals("0"))
				{
				Intent intent_details = new Intent(context,
						ImageViewActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("jsonObject", _userList.get((Integer) v.getTag()).toString());
//				String image_path = null;
//				try {
//					image_path = _userList.get((Integer) v.getTag())
//							.getString(Constants.kprofile_image_path);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				bundle.putString("imagePath", image_path);
				intent_details.putExtras(bundle);
				context.startActivity(intent_details);
				}
				else if(type.equals("0")){
					Intent intent_details = new Intent(context,
							Activity_SimpleDoormanView.class);
					Bundle bundle = new Bundle();
					bundle.putString("jsonObject", _userList.get((Integer) v.getTag()).toString());
					intent_details.putExtras(bundle);
					context.startActivity(intent_details);
				}
			}
		});

		// check if is new the make background red
		try {
			String userId = _userList.get(position).getString(
					Constants.kuser_id);
			
			//get previous users states(if they need red border or not)
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
			String testString= prefs.getString(Constants.kCurrentUserStates, null);
			JSONObject userPreStates =null;
			if(testString!=null){
				// How to retrieve your Java object back from the string
				Gson gson = new Gson();
				Type collectionType = new TypeToken<JSONObject>(){}.getType();
				userPreStates = gson.fromJson(testString, collectionType);
				
				if(_userList.get(position).getString(Constants.kcheckinout).equals("1"))
					rl_image_frame.setBackgroundColor(Color.GREEN);
				else if (isRedBgNeeded(userPreStates,userId)) {
						rl_image_frame.setBackgroundColor(Color.RED);
				}
			}
			
			
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		// String voice_path=null;
		try {
			String file_path = _userList.get(position).getString(
					Constants.kuser_name_recording_path);
			String user_id = _userList.get(position).getString(
					Constants.kuser_id);
			String[] split = file_path.split("upload_voice");
			String filenamewithextention = split[1];

			StringTokenizer tokens = new StringTokenizer(
					filenamewithextention, ".");
			String _filename = tokens.nextToken();

			String file_name = _filename + "_" + user_id + ".mp3";
			File f = new File("/sdcard/doorman", file_name);
			if (!f.exists()) {
				FileDownloader fileDownloader = new FileDownloader();
				fileDownloader.callBack = this;
				fileDownloader.fileName = file_name;
				fileDownloader.filePath = "/doorman";
				fileDownloader.execute(file_path);

				// check for previous voice file for same user, if there is
				// then delete it and download, if not then simply download
				File doormanDir = new File("/sdcard/doorman");
				for (File file : doormanDir.listFiles()) {
					if (file.isFile()) {
						String name = file.getName();
						String[] _split = name.split("_");
						String _user_id = _split[1];
						StringTokenizer _tokens = new StringTokenizer(
								_user_id, ".");
						_user_id = _tokens.nextToken();
						if (user_id.endsWith(_user_id)) {
							File _f = new File("/sdcard/doorman/", name);
							if (_f.exists()) {
								_f.delete();
							}
						}
					}
				}

			}

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			Log.d("file exception", "my file exception");
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			Log.d("voice file not found for user",
					"voice file not found for user");
			e1.printStackTrace();
		}

//		try {
//			final Button btn_checkIn = (Button) convertView
//					.findViewById(R.id.btn_checkIn);
//			SharedPreferences settings = PreferenceManager
//					.getDefaultSharedPreferences(context);
			
//			String type=settings.getString("type", null);
//			if(type.equals("0")){
//				btn_checkIn.setVisibility(View.INVISIBLE);
//			}else{
//			btn_checkIn.setFocusable(false);
//			btn_checkIn.setTag(position);
//			if(_userList.get(position).getString(Constants.kcheckinout).equals("0"))
//				btn_checkIn.setText("check Out");
//			else
//				btn_checkIn.setText("check In");
//			}
//			registerAndSetListener(btn_checkIn,position,convertView);
//		} catch (NumberFormatException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		} catch (Exception e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
		
		Button btn_play = (Button) convertView
				.findViewById(R.id.btn_play_voice);
		btn_play.setFocusable(false);
		btn_play.setTag(position);
		btn_play.setOnClickListener(new View.OnClickListener() {

			@SuppressLint("SdCardPath")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int i = (Integer) v.getTag();
				String file_path = null;
				try {
					file_path = _userList.get(i).getString(
							Constants.kuser_name_recording_path);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				String user_id = null;
				try {
					user_id = _userList.get(i)
							.getString(Constants.kuser_id);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {

					String[] split = file_path.split("upload_voice");
					String filenamewithextention = split[1];

					StringTokenizer tokens = new StringTokenizer(
							filenamewithextention, ".");
					String _filename = tokens.nextToken();

					String voice_path = _filename + "_" + user_id + ".mp3";
					 
					f = new File("/sdcard/doorman", voice_path);

					// if file exist and no other file is playing
					if (f.exists() && !audioPlaying) {
						// Log.e("s0und path",""+userDataList.get((Integer)
						// v.getTag()).user_Id+"_voice.mp3"+(Integer)
						// v.getTag() );
						// Log.e("s0und path",""+userDataList.get((Integer)
						// v.getTag()).voice_recording_image_path);
						Intent intent = new Intent(context,
								AudioService.class);
						intent.putExtra("audio",f.getAbsolutePath()); 
						context.startService(intent);
//						Thread myThread = new Thread(new Runnable() {
//
//							@Override
//							public void run() {
//								
//								
//					
//
//						audioPlaying = true;
//						try {
//							mp = new MediaPlayer();
//							mp.setDataSource(f.getAbsolutePath());
//							mp.prepare();
//						} catch (IllegalArgumentException e) {
//							e.printStackTrace();
//						} catch (IllegalStateException e) {
//							e.printStackTrace();
//						} catch (IOException e) {
//							e.printStackTrace();
//						}
//						mp.start();
//						mp.setOnCompletionListener(new OnCompletionListener() {
//
//							@Override
//							public void onCompletion(MediaPlayer mp) {
//								// TODO Auto-generated method stub
//								audioPlaying = false;
//								mp.release();
//							}
//						});
//							}
//						});
//						myThread.start();
					}
				} catch (Exception e) {
					// TODO: handle exception
					Log.d("voice file not found for user",
							"voice file not found for user");
					e.printStackTrace();
				}

			}

		});

		ImageLoader imgLoader = new ImageLoader(context);
		try {
//			DecimalFormat df = new DecimalFormat();
//			df.setMaximumFractionDigits(2);
			imgLoader.DisplayImage(
					_userList.get(position).getString(
							Constants.kprofile_image_path), img_view);
			tv_username.setText(_userList.get(position).getString(
					Constants.kfirst_name)
					+ " "
					+ _userList.get(position).getString(
							Constants.klast_name));
//			tv_distance.setText(df.format(Float.parseFloat(_userList.get(
//					position).getString(Constants.kdistance)))
//					+ "meters");
//			tv_like.setText("Like/Dislike: "
//					+ _userList.get(position).getString(Constants.klike));
//			tv_reassOn.setText("Reasson: "
//					+ _userList.get(position).getString(Constants.kreason));
//			tv_title.setText("Title:"
//					+ _userList.get(position).getString(Constants.ktitle));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return convertView;
	}

//void registerAndSetListener(final Button btn_checkIn,final int position,View convertView)
//{
//
////	btn_checkIn.setText("check In");	
//	btn_checkIn.setOnClickListener(new View.OnClickListener() {
//		
//		@Override
//		public void onClick(View v) {
//			// TODO Auto-generated method stub
//			try {
//				if(_userList.get(position).getString(Constants.kcheckinout).equals("0")){
//					btn_checkIn.setText("check In");
//					checkInAndCheckOutSendRequest("1",position);
//				}
//				else{
//					btn_checkIn.setText("check Out");	
//					checkInAndCheckOutSendRequest("0",position);
//				}
//
//			} catch (NumberFormatException e2) {
//				// TODO Auto-generated catch block
//				e2.printStackTrace();
//			} catch (Exception e2) {
//				// TODO Auto-generated catch block
//				e2.printStackTrace();
//			}
//		}
//	});
//	
//}
	@Override
	public void fileDownloaded(FileDownloaderInterface callBack) {
		// TODO Auto-generated method stub
		Log.d("file", "file downloaded");
	}
//void checkInAndCheckOutSendRequest(final String str,final int position){
//	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
//	String key = settings.getString("key", null);
//	String type = settings.getString("type", null);
//	JSONObject obj = new JSONObject();
//	try {
//		obj.put("check_in_out",str);
//		obj.put("api_key", key);
//		obj.put("user_id",_userList.get(position).getString(Constants.kuser_id));
//		obj.put("employee_type",type);
//	} catch (JSONException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
//	RequestParams params = new RequestParams();
//	params.put("data",obj.toString());
//	AsyncHttpClient client = new AsyncHttpClient();
//	client.post(Constants.api_base_url+Constants.change_check_in_out_status, params , new JsonHttpResponseHandler(){
//		@Override
//        public void onSuccess(final JSONObject object){
//			int status = 0;
//			try {
//				status = object.getInt("status");
//					if (status == 1){
//						_userList.get(position).remove(Constants.kcheckinout);
//						_userList.get(position).put(Constants.kcheckinout, str);
//						Toast.makeText(context , "Check Out Update" , Toast.LENGTH_SHORT).show();
//					}
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//	     @Override
//         protected void handleFailureMessage(Throwable e, String responseBody) {
//				try{
//				JSONObject jobject =  new JSONObject(responseBody);
//				int status = jobject.getInt("status");
//				if(status ==0){
//					Toast.makeText(context , "Check not Update" , Toast.LENGTH_SHORT).show();
//
//				}
//			} catch (JSONException ex) {
//				// TODO Auto-generated catch block
//				ex.printStackTrace();
//			}
//         }
//	});
//	
//}
}
