package com.tbox.doormanapp;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;


@SuppressLint("SdCardPath")
public class FileDownloader extends AsyncTask<String, String, String> {
	public String filePath=null;
	public String fileName=null;
	public FileDownloaderInterface callBack=null;
	@SuppressLint("SdCardPath")
	@Override
	protected String doInBackground(String... f_url) {
		// TODO Auto-generated method stub
		int count;
		try{
			URL url = new URL(f_url[0]);
	         URLConnection conection = url.openConnection();
	         conection.connect();
	                // download the file
	         InputStream input = new BufferedInputStream(url.openStream(), 8192);

	         // Output stream
	         File f = new File("/sdcard", filePath);
	         f.mkdirs();
	         OutputStream output = new FileOutputStream(String.format("/sdcard/%s/%s",filePath,fileName));

	         byte data[] = new byte[2048];


	         while ((count = input.read(data)) != -1) {
	             // writing data to file
	             output.write(data, 0, count);
	         }

	         // flushing output
	         output.flush();
	         // closing streams
	         output.close();
	         input.close();			
		}catch (Exception e) {
         Log.e("Error: ", e.getMessage());
     }

     return null;
	}
	
	/**
     * After completing background task
     * Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {
    	
    	try {
    		if(callBack!=null){
    			callBack.fileDownloaded(callBack);
    		}
    		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	}
}
