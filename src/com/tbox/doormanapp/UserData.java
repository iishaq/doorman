package com.tbox.doormanapp;

public class UserData{

	String userName;
	String longitude;
	String latitude;
	String distance;
	public String profile_image_path;
	public String voice_recording_image_path;
	
	String status;
	int user_Id;
	public void setUserName (String userName){
		this.userName = userName;
		
	}
	public void setUserId(int user_Id){
		this.user_Id = user_Id;
		
	}
	public String getUserName (){
		return userName ;
		
	}
	public void setLongitude (String longitude){
		this.longitude = longitude;
		
	}
	public String getLongitude (){
		return longitude ;
		
	}
	public void setLatitude (String latitude){
		this.latitude = latitude;
		
	}
	public String getLatitude (){
		return latitude;
		
	}
	public void setDistance (String distance){
		this.distance= distance;
		
	}
	public String getDistance (){
		return distance;
		
	}
	public void setStatus (String status){
		this.status= status;
		
	}
	public String getStatus (){
		return status;
		
	}
	public int getUserId (){
		return user_Id;
		
	}
	/*public void setUserVoice (String voice){
		this.userName = voice;
		
	}*/

}
