package com.tbox.doormanapp;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.Vibrator;
import android.os.PowerManager.WakeLock;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

@SuppressWarnings("deprecation")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class ServiceDoorman extends Service{
	static Context context;
	final static String UPDATE_LIST_ACTION = "UPDATE_LIST_ACTION";
	static ArrayList<JSONObject> currentUserList = null;
	static JSONObject newUserStates = new JSONObject();
	JSONObject changedUserStates = null;
	static AlertSoundPlayer alertSoundPlayer = null; 
	static GuestListActivity listActivity=null;
	Timer timer;
	TimerTask timertask;
//	ServiceDoorman(){
//		super();
//	}
	@Override
	public void onCreate(){
		super.onCreate();
		Toast.makeText(getApplicationContext(), "Service Created", 1).show();
//		context= this.getApplicationContext();
//		if(currentUserList == null){
//			currentUserList = new ArrayList<JSONObject>();
//		}
//		 
//		changeUserState();
		
	}
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@SuppressWarnings("deprecation")
	public void manageNewUserState(){
		
		//get previous users states(if they need red border or not)
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ServiceDoorman.this);
		String testString= prefs.getString(Constants.kCurrentUserStates, null);
		JSONObject userPreStates =null;
		if(testString==null){
			userPreStates = new JSONObject();
		}else{
			// How to retrieve your Java object back from the string
			Gson gson = new Gson();
			Type collectionType = new TypeToken<JSONObject>(){}.getType();
			userPreStates = gson.fromJson(testString, collectionType);
		}
		
		JSONObject userCurrStates = new JSONObject();
		boolean isPlaying = false;
		for (int i = 0; i < currentUserList.size(); i++) {
			
			String _current_user_id=null;
			try {
				_current_user_id = currentUserList.get(i).getString(Constants.kuser_id);
				if(userPreStates.isNull(_current_user_id)){
					userCurrStates.put(_current_user_id, System.currentTimeMillis());
					
								         
//					KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE); 
//			        KeyguardLock keyguardLock =  keyguardManager.newKeyguardLock("TAG");
//			        keyguardLock.disableKeyguard();
////			        keyguardManager.exitKeyguardSecurely(null);
//			        
//					PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
//
//			         boolean isScreenOn = pm.isScreenOn();
//
////			         Log.e("screen on.................................", ""+isScreenOn);
//
//			         if(isScreenOn==false)
//			         {
//
//			        WakeLock wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK |PowerManager.ACQUIRE_CAUSES_WAKEUP |PowerManager.ON_AFTER_RELEASE,"MyLock");
//
//			                            wl.acquire(10000);
//			        WakeLock wl_cpu = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,"MyCpuLock");
//
//			                 wl_cpu.acquire(10000);
//			         }
//					 
////					if(alertSoundPlayer!=null){
////						if(alertSoundPlayer.isPlaying==false){
////							alertSoundPlayer = new AlertSoundPlayer();
////					         alertSoundPlayer.context=context;
////					         alertSoundPlayer._user=(JSONObject)currentUserList.get(i);
////					         alertSoundPlayer.isPlaying=true;
////					         alertSoundPlayer.playAlertSoundForVoice(alertSoundPlayer._user, context);
////						}
////					}else{
//			         
//			         if(isPlaying==false){
//			        		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
//			    			String type=settings.getString("type", null);
//			        	 if(type.equals("2")){
//			        		 if(currentUserList.get(i).getString(Constants.ksuper_user).equals("1")){
//			        			 isPlaying=true;
//			        			 alertSoundPlayer = new AlertSoundPlayer();
//			        			 alertSoundPlayer.context=context;
//			        			 alertSoundPlayer._user=(JSONObject)currentUserList.get(i);
//			        			 alertSoundPlayer.playAlertSoundForVoice(alertSoundPlayer._user, context,currentUserList.get(i).getString(Constants.kcheckinout)); 
//			        		 }
//			        	 }else{
//				        	 	isPlaying=true;
//				        	 	alertSoundPlayer = new AlertSoundPlayer();
//								alertSoundPlayer.context=context;
//						        alertSoundPlayer._user=(JSONObject)currentUserList.get(i);
//						        alertSoundPlayer.playAlertSoundForVoice(alertSoundPlayer._user, context,currentUserList.get(i).getString(Constants.kcheckinout)); 
//
//			        	 }
//			         }
//						
////					}
			         
				}
				else{
					userCurrStates.put(_current_user_id, userPreStates.getString(_current_user_id));
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		// How to store JSON string
		Gson gson = new Gson();
		String json = gson.toJson(userCurrStates);
		
		Editor editor = prefs.edit();
		editor.putString(Constants.kCurrentUserStates, json);
		editor.commit();
	}
	
	private void getUsersList(){
//		if(timer != null){
//			timer.cancel();
//		}
//	    timer = new Timer();
//	    timertask = new TimerTask(){ @SuppressLint("SdCardPath")
//		public void run() {
//		
	    	SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ServiceDoorman.this);
	 		String key = settings.getString("key", null);
	 		String property_id = settings.getString("property_id", null);	 		
	 		String type=settings.getString("type", null);
	 		
			RequestParams params = new RequestParams();
			params.put("api_key",key);
			params.put("property_id",property_id);
			params.put("employee_type", type);
			
	      //send post request for user login
	    	AsyncHttpClient client = new AsyncHttpClient();
	    	client.get(Constants.api_base_url+Constants.guest_status, params , new JsonHttpResponseHandler(){
				@SuppressWarnings("unchecked")
				@SuppressLint({ "CommitPrefEdits", "Wakelock" })
				@Override
	            public void onSuccess(final JSONObject object){
					
					Log.e("responce",object.toString());
					try {
						int status=object.getInt("status");
						if (status == 1){
							JSONArray array=object.getJSONArray("users");
							//if no user is coming from backend, make an empty array and load to the grid view
							if(array==null){
								
								// How to store JSON string
								Gson gson = new Gson();
								// This can be any object. Does not have to be an arraylist.
								ArrayList<JSONObject> userList = new ArrayList<JSONObject>();
								String json = gson.toJson(userList);
								
								SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ServiceDoorman.this);
								Editor editor = prefs.edit();
								editor.putString(Constants.kCurrentUsersList, json);
								editor.commit();
								
//								if(MyApplication.isActivityVisible()){
									Intent intent = new Intent();
						    		intent.setAction(UPDATE_LIST_ACTION);
						    		sendBroadcast(intent);	
//						    	}
								return;
							}
							//else get users list and check if there is any new user, make an alert and update the grid view
							
							ArrayList<JSONObject> userList = new ArrayList<JSONObject>();
							for(int i=0;i<array.length();i++){
								userList.add(array.getJSONObject(i));
							}
							SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ServiceDoorman.this);
							String testString= prefs.getString(Constants.kCurrentUsersList, null);
							
							// How to retrieve your Java object back from the string
							Gson gson = new Gson();
							Type collectionType = new TypeToken<ArrayList<JSONObject>>(){}.getType();
							currentUserList = gson.fromJson(testString, collectionType);
							if(currentUserList==null){
								currentUserList = new ArrayList<JSONObject>();
							}
//							if(currentUserList.size()==0 && userList.size()>0){
//								currentUserList=userList;
////								ServiceDoorman.manageNewUserState();
////								sendNotification();
//							}else{
//								//check if new user
								boolean isNewUser=false;
								for(int i = 0 ; i < userList.size() ;i++){
									boolean isExist=false;
									for(int j = 0 ; j < currentUserList.size() ;j++)
									{
										String _user_id = userList.get(i).getString(Constants.kuser_id);
										String _current_user_id = currentUserList.get(j).getString(Constants.kuser_id);
										if(_user_id.equalsIgnoreCase(_current_user_id)){
											isExist=true;
										}
									
									}
									if(isExist == false){
										isNewUser=true;
										alertSoundPlayer = new AlertSoundPlayer();
										alertSoundPlayer.context=context;
										alertSoundPlayer._user=(JSONObject)userList.get(i);
										alertSoundPlayer.playAlertSoundForVoice(alertSoundPlayer._user, ServiceDoorman.this,userList.get(i).getString(Constants.kcheckinout));
										
										KeyguardManager km = (KeyguardManager) ServiceDoorman.this.getSystemService(Context.KEYGUARD_SERVICE); 
										final KeyguardManager.KeyguardLock kl = km .newKeyguardLock("MyKeyguardLock"); 
										kl.disableKeyguard(); 
										
										PowerManager pm = (PowerManager) ServiceDoorman.this.getSystemService(Context.POWER_SERVICE); 
										WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
										//acquire wake lock with timeout
										wakeLock.acquire(1000); 
										sendNotification();
										
//										alertSoundPlayer.isPlaying=true;
//										alertSoundPlayer.playAlertSoundForVoice(alertSoundPlayer._user, context);
										break;
									}
								}
								currentUserList=userList;
								manageNewUserState();
							
							//save current users and update gridview
							String json = gson.toJson(currentUserList);
							Editor editor = prefs.edit();
							editor.putString(Constants.kCurrentUsersList, json);
							editor.commit();
							
//							if(MyApplication.isActivityVisible()){
								Intent intent = new Intent();
					    		intent.setAction(UPDATE_LIST_ACTION);
					    		sendBroadcast(intent);			
//					    	}
						}else{
							// How to store JSON string
							Gson gson = new Gson();
							// This can be any object. Does not have to be an arraylist.
							currentUserList = new ArrayList<JSONObject>();
							String json = gson.toJson(currentUserList);
							
							SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ServiceDoorman.this);
							Editor editor = prefs.edit();
							editor.putString(Constants.kCurrentUsersList, json);
							editor.commit();
							
							
							manageNewUserState();
							Intent intent = new Intent();
				    		intent.setAction(UPDATE_LIST_ACTION);
				    		sendBroadcast(intent);
						}

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Log.d("api", "api error1");
						e.printStackTrace();
					}
	    		}

			    public void onFailure(Throwable e, JSONObject errorResponse) {
			    
			    	Log.d("testing",errorResponse.toString());
			    	
			    }
			    public void onFailure(Throwable e, JSONArray errorResponse) {
			    	Log.d("api", "api failure");
			    }

	    	
	    	});
//	    }};	
//	timer.scheduleAtFixedRate(timertask,500, 15000);
		
		
	}

	@SuppressLint("ShowToast")
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		int retVal = super.onStartCommand(intent, flags, startId);
		Toast.makeText(this, "on start command", Toast.LENGTH_LONG).show();
		this.getUsersList();

//		this.stopSelf();

	    	return retVal;
	}
	
	
    @Override
    public void onDestroy() {
        Toast.makeText(this, "MyService Stopped", Toast.LENGTH_LONG).show();
        if(timer!=null){
        	timer.cancel();
        }
        this.stopSelf();
    }
    
    // call this code after setting new currentUserList
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@SuppressLint({ "NewApi", "Wakelock" })
	public void sendNotification(){
    	
    	

        Intent intent= new Intent(ServiceDoorman.this, GuestListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
////    	if(!MyApplication.isActivityVisible()){
//    		Intent _intent = new Intent();
//    		_intent.setAction(UPDATE_LIST_ACTION);
//    		sendBroadcast(_intent);			
////    	}
    	// Build notification
//    	Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//    	Uri path = Uri.parse("android.resource://"+getPackageName()+ "/"+ R.raw.testingfinal);
    	Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

//        Intent notificationIntent = new Intent(this, GuestListActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
//        Notification.Builder noti = new Notification.Builder(this)
//        .setContentTitle("Uber Guest")
//        .setContentText("New Guest Has Arrived!")
//        .setContentIntent(contentIntent)
//        .setSmallIcon(R.drawable.ic_launcher) 
////        .setSound(path)
//        .setOnlyAlertOnce(false)
//        .setAutoCancel(true);
        v.vibrate(500);
//    	NotificationManager notificationManager = 
//    			  (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//    			notificationManager.notify(1, noti.getNotification());
    			
    }
	
    
    
}
