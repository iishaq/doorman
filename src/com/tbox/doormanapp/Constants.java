package com.tbox.doormanapp;

public class Constants {
	
	static public String kCurrentUsersList		= "current_user_list";
	static public String kCurrentUserStates		= "current_user_states";
	
	static public String api_base_url = "http://www.uberguest.com/uberguest_test/index.php/api/";
//	static public String api_base_url = "http://www.uberguest.com/uberguest_dev/index.php/api/";
//	static public String api_base_url = "http://www.uberguest.com/uberguest_res/index.php/api/";
	//api methods
	static public String doorman_login = "employee_login";
	static public String guest_status = "guest_status";
	static public String guest_visit_record =  "guest_visits_records";
	static public String change_check_in_out_status =  "change_check_in_out_status";
	
	static public String kcity                  = "city";
	static public String kcountry               = "country";
	static public String kfirst_name 			= "first_name";
	static public String klast_name 			= "last_name";
	static public String klongitude				= "longitude";
	static public String klatitude 				= "latitude";
	static public String kdistance 				= "distance";
	static public String kstatus 				= "status";
	static public String kuser_id 				= "user_id";
	static public String ktitle                 = "title";
	static public String kcompany               = "company";
	static public String kreason                = "reason_for_visit";
	static public String kcomment 				= "comment";
	static public String kdate 					= "comment_date";
	static public String khotel_name 			= "hotel_name";
	static public String ktotal_point 			= "points";
	static public String klike                  = "likes";
	static public String kdislike               = "dislikes";
	static public String kspouse                = "spouse";
	static public String kbirthday              = "birthday";
	static public String kspecial_instruction 	= "special_instructions";
	static public String kcell_number 			= "cell_number";
	static public String kcheckinout			= "check_in_out";
	static public String ksuper_user			= "super_user";
	static public String kprofile_image_path 	= "profile_image";//to do, change the key to be profile_image_path
	static public String kuser_name_recording_path = "voice";//to do, change the key to be user_name_recording_path
}
