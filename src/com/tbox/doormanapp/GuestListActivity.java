package com.tbox.doormanapp;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.doorman_test.R;

public class GuestListActivity extends Activity implements OnClickListener{

	ArrayList<JSONObject> userList = null;
	Button btnLogout;
	boolean audioPlaying = false;
	MediaPlayer mp = new MediaPlayer();
	int previouscolor;
	DoormanServiceReceiver myReceiver;
	PendingIntent pintentService;
	GridView gridView;
	GridViewAdapterGuest gridviewAdapter;

	private void startService() {
//		if (!isServiceRunning(GuestListActivity.this, "ServiceDoorman")) {
//			if (isNetworkAvailable()) {
				Intent myAlarm = new Intent(getApplicationContext(), MyReceiver.class);
				//myAlarm.putExtra("project_id", project_id); //Put Extra if needed
				PendingIntent recurringAlarm = PendingIntent.getBroadcast(getApplicationContext(), 0, myAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
				AlarmManager alarms = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
				Calendar updateTime = Calendar.getInstance();
				//updateTime.setWhatever(0);    //set time to start first occurence of alarm 
				alarms.setInexactRepeating(AlarmManager.RTC_WAKEUP, updateTime.getTimeInMillis(), 30000, recurringAlarm); //you can modify the interval of course
////				ServiceDoorman.currentUserList = userList;
//				Intent intent = new Intent(GuestListActivity.this, ServiceDoorman.class);
//				PendingIntent pintent = PendingIntent.getService(GuestListActivity.this, 0, intent, 0);
//				AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//				alarm.setRepeating(AlarmManager.RTC_WAKEUP,1000,10000, pintent);				
//			}
//		}

	}

	private void fetchGuesList() {

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);
		String key = settings.getString("key", null);
		String property_id = settings.getString("property_id", null);
		String type=settings.getString("type", null);
		RequestParams params = new RequestParams();
		params.put("api_key", key);
		params.put("property_id", property_id);
		params.put("employee_type", type);

		// send post request for user login
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constants.api_base_url + Constants.guest_status, params,
				new JsonHttpResponseHandler() {
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(final JSONObject object) {
						try {
							int status = object.getInt("status");
							if (status == 1) {
								JSONArray array = object.getJSONArray("users");
								if (array == null) {
									return;
								}
								userList = new ArrayList<JSONObject>();
								for (int i = 0; i < array.length(); i++) {
									userList.add(array.getJSONObject(i));
								}
								ServiceDoorman.currentUserList = userList;
//								ServiceDoorman.manageNewUserState();
								if (gridviewAdapter == null){
//									listAdapter = new DoormanListAdapter(
//											GuestListActivity.this,
//											R.layout.guest_list_activity,
//											userList);
//									setListAdapter(listAdapter);
									gridviewAdapter = new GridViewAdapterGuest(GuestListActivity.this,
											R.layout.guest_list_activity,
											userList);
									
									gridView.setAdapter(gridviewAdapter);
								} else {
									gridView.invalidateViews();
//									((ArrayAdapter<JSONObject>) listAdapter)
//											.notifyDataSetChanged();
								}
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							Log.d("api", "api error");
							e.printStackTrace();
						}
					}
				});

	}

	private void registerReciver() {
		// Register BroadcastReceiver
		// to receive event from our service
		myReceiver = new DoormanServiceReceiver();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(ServiceDoorman.UPDATE_LIST_ACTION);
		registerReceiver(myReceiver, intentFilter);
	}

	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.listgueststatus);
		registerwidgets();
		registerReciver();
		startService();
//		ServiceDoorman.listActivity = this;
//		fetchGuesList();
		

	}
	
	private void registerwidgets(){
		gridView = (GridView) findViewById(R.id.gridView1);
		btnLogout = (Button) findViewById(R.id.btnLogout);
		btnLogout.setOnClickListener(this);
	}
	 
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public boolean isServiceRunning(Context ctx, String serviceClassName) {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (ServiceDoorman.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onPause()
	 */
	@SuppressWarnings("deprecation")
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		// MyApplication.activityPaused();
		super.onPause();
		MyApplication.activityPaused();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MyApplication.activityResumed();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.ListActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(myReceiver);
		super.onDestroy();
	}

	private class DoormanServiceReceiver extends BroadcastReceiver {

		@SuppressWarnings("unchecked")
		@Override
		public void onReceive(Context arg0, Intent arg1) {
			// TODO Auto-generated method stub
			
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(GuestListActivity.this);
			String testString= prefs.getString(Constants.kCurrentUsersList, null);
			
			// How to retrieve your Java object back from the string
			Gson gson = new Gson();
			Type collectionType = new TypeToken<ArrayList<JSONObject>>(){}.getType();
			userList = gson.fromJson(testString, collectionType);
			
		
//			userList = new ArrayList<JSONObject>();
//			 = ServiceDoorman.currentUserList;
//			listAdapter = new DoormanListAdapter(GuestListActivity.this,
//					R.layout.guest_list_activity, userList);
//			setListAdapter(listAdapter);
			gridviewAdapter = new GridViewAdapterGuest(GuestListActivity.this,
					R.layout.guest_list_activity,
					userList);
			
			gridView.setAdapter(gridviewAdapter);
			gridView.invalidateViews();
//			Toast.makeText(GuestListActivity.this, "reload Grid view", Toast.LENGTH_LONG).show();
//			((BaseAdapter) listAdapter).notifyDataSetChanged();
		}

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()){
		case R.id.btnLogout:
			SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(GuestListActivity.this);
        	settings.edit().clear().commit();
        	GuestListActivity.this.stopService(new Intent(GuestListActivity.this, ServiceDoorman.class));
        	Intent intent_logout =new Intent(GuestListActivity.this,LoginActivity.class);
			startActivity(intent_logout);
			finish();
		break;	
		}
	}
}
