package com.tbox.doormanapp;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.IBinder;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class AudioService extends Service{
	MediaPlayer mp ;
	Context con;
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
@SuppressWarnings("deprecation")
@Override
public void onStart(Intent intent, int startId) {
	// TODO Auto-generated method stub
	super.onStart(intent, startId);
	//Toast.makeText(getApplicationContext(), "service start", 2).show();
	con=this;
	try {
		 mp = new MediaPlayer();
		mp.setDataSource((String) intent.getSerializableExtra("audio"));
		mp.prepare();
	} catch (IllegalArgumentException e) {
		e.printStackTrace();
	} catch (IllegalStateException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	mp.start();
	mp.setOnCompletionListener(new OnCompletionListener() {

		@Override
		public void onCompletion(MediaPlayer mp) {
			// TODO Auto-generated method stub
		
			mp.release();
			stopSelf();
		}
	});
}


@Override
public void onDestroy() {
	// TODO Auto-generated method stub
	super.onDestroy();
	//Toast.makeText(getApplicationContext(), "service destroyed", 2).show();
}
}
