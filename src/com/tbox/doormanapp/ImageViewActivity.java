package com.tbox.doormanapp;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.tbox.doorman_test.R;
import com.tbox.imagedownloader.ImageLoader;
import com.tbox.imagedownloader.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ImageViewActivity extends Activity{
	ImageView image_view;
	TextView tv_username,tv_distance,tv_like,tv_company,tv_title,tv_point,tv_firstName,tv_lastName,tv_birthday,tv_spouseName,
		tv_city,tv_country,tv_dislike,tv_speial_instruction ,tv_cell_number;
	ListView lv;
	ArrayList<LastVisit> arrLastVisit;
	float value;
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_image_view_activity);
        registerwidgets();
        value = 13 * getResources().getDisplayMetrics().density;
        JSONObject 	jsonObj = null;
		try {
			jsonObj = new JSONObject(getIntent().getStringExtra("jsonObject"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(jsonObj!=null)
			setDormanDetails(jsonObj);
	}
	
	private void setDormanDetails(JSONObject jsonOject){
		DecimalFormat df = new DecimalFormat();
		df.setMaximumFractionDigits(2);
		ImageLoader imgLoader = new ImageLoader(ImageViewActivity.this);
		try {
			imgLoader.DisplayImage(jsonOject.getString(Constants.kprofile_image_path),image_view);
//			tv_username.setText(jsonOject.getString(Constants.kfirst_name)
//					+ " "
//					+ jsonOject.getString(Constants.klast_name));

			tv_firstName.setText("First Name: "+ jsonOject.getString(Constants.kfirst_name));
			//tv_firstName.setTextSize(value);
			
			tv_lastName.setText("Last Name: "+ jsonOject.getString(Constants.klast_name));
			//tv_lastName.setTextSize(value);
			
			tv_spouseName.setText("Spouse Name: "+ jsonOject.getString(Constants.kspouse));
			//tv_spouseName.setTextSize(value);
			
			tv_birthday.setText("Birthday: "+ jsonOject.getString(Constants.kbirthday));
			//tv_birthday.setTextSize(value);
			
			tv_speial_instruction.setText(jsonOject.getString(Constants.kspecial_instruction));
			//tv_speial_instruction.setTextSize(value);
			
			tv_cell_number.setText("Cell Numnber: "+jsonOject.getString(Constants.kcell_number));
			//tv_cell_number.setTextSize(value);
			
			tv_like.setText("Likes: "+ jsonOject.getString(Constants.klike));
			//tv_like.setTextSize(value);
			
			tv_company.setText("Company: "+ jsonOject.getString(Constants.kcompany));
			//tv_company.setTextSize(value);
			
			tv_dislike.setText("Dislikes: "+ jsonOject.getString(Constants.kdislike));
			//tv_dislike.setTextSize(value);
			
			tv_country.setText("Country: "+ jsonOject.getString(Constants.kcountry));
			//tv_country.setTextSize(value);
			
     		tv_city.setText("City: "+ jsonOject.getString(Constants.kcity));
     		//tv_city.setTextSize(value);
     		
			tv_title.setText("Title: "+ jsonOject.getString(Constants.ktitle));
			//tv_title.setTextSize(value);
			tv_point.setText("Total Points:          "+ jsonOject.getString(Constants.ktotal_point));
			//tv_point.setTextSize(value);
			getLastVisit(jsonOject.getString(Constants.kuser_id));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void registerwidgets(){
        image_view = (ImageView)findViewById(R.id.imageViewActivity);
        tv_username = (TextView)findViewById(R.id.guestUserName);
		tv_distance = (TextView)findViewById(R.id.distanceGuest);
		tv_like = (TextView)findViewById(R.id.like);
		tv_city = (TextView)findViewById(R.id.city);
		tv_country = (TextView)findViewById(R.id.country);
		tv_dislike = (TextView)findViewById(R.id.disLike);
		tv_firstName = (TextView)findViewById(R.id.firstname);
		tv_lastName = (TextView)findViewById(R.id.lastname);
		tv_country = (TextView)findViewById(R.id.country);
		tv_spouseName= (TextView)findViewById(R.id.spouseName);
		tv_birthday= (TextView)findViewById(R.id.birthday);
		tv_speial_instruction = (TextView)findViewById(R.id.tvSpecialInstruction);
		tv_cell_number = (TextView)findViewById(R.id.cellNumber);
		tv_title = (TextView)findViewById(R.id.title);
		lv = (ListView)findViewById(R.id.lvLastVisit);
		tv_point = (TextView)findViewById(R.id.totalPoint);
		tv_company = (TextView)findViewById(R.id.company);
		arrLastVisit = new ArrayList<LastVisit>();
	}
	

	class ListAdapter1 extends ArrayAdapter<LastVisit>{

	 	public ListAdapter1() {
	 		super(ImageViewActivity.this, R.layout.inflate_last_visit,arrLastVisit);
	 		// TODO Auto-generated constructor stub
	 	}

		/* (non-Javadoc)
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView==null){
				LayoutInflater	inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.inflate_last_visit, null);
 		    }
 			TextView hotelName = (TextView)convertView.findViewById(R.id.hotelName);
 			TextView date = (TextView)convertView.findViewById(R.id.date);
 			TextView comment = (TextView)convertView.findViewById(R.id.comments);
 			comment.setMovementMethod(new ScrollingMovementMethod());
 			hotelName.setText(arrLastVisit.get(position).hotelName);
 			//hotelName.setTextSize(value);
 			date.setText( arrLastVisit.get(position).date);
 			//date.setTextSize(value);
 			comment.setText( arrLastVisit.get(position).comment);
 			//comment.setTextSize(value);
 			//line.setHeight(comment.getHeight());
 			
 			return convertView;	
		}
	 	

	}
	private void getLastVisit(String user_id){
		RequestParams params = new RequestParams();
		params.put("user_id", user_id);
		// send post request for visit_record
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constants.api_base_url + Constants.guest_visit_record, params,
				new JsonHttpResponseHandler() {
					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(final JSONObject object) {
						try {
							int status = object.getInt("status");
							if (status == 1) {
								JSONArray array = object.getJSONArray("visits_records");
								if (array == null) {
									return;
								}else{
									for (int i = 0; i < array.length(); i++) {
										LastVisit la = new LastVisit();
										la.hotelName = array.getJSONObject(i).getString(Constants.khotel_name);
										la.date = array.getJSONObject(i).getString(Constants.kdate);
										la.comment = array.getJSONObject(i).getString(Constants.kcomment);
										arrLastVisit.add(la);
									}
									ListAdapter1 listAdapter=new ListAdapter1();
									lv.setAdapter(listAdapter);
								}
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							Log.d("api", "api error");
							e.printStackTrace();
						}
					}
		             @Override
		             protected void handleFailureMessage(Throwable e, String responseBody) {
//		            	 spinner.setVisibility(View.INVISIBLE);
		 				try{
							JSONObject jobject =  new JSONObject(responseBody);
							int status = jobject.getInt("status");
							if(status ==0){
//								UserInfo userInfo= UserInfo.getInstance(context);	
								showAlertDialog(jobject.getString("error_message"), ImageViewActivity.this);
//								userInfo.saveUserInfo(context);
							}
						} catch (JSONException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
		             }
				});
	}
	static public void showAlertDialog(String message, Context context)
	 {
		 new AlertDialog.Builder(context)
		 .setMessage(message)
		 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
			 public void onClick(DialogInterface dialog, int which) {
				 
			 }
		 	})
		 	.show();
	 }
	

}
